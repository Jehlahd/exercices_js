/*
 
Consignes de l'exercice

Créer une fonction qui prend deux valeurs en entrée et écrit dans la console si
leur type est identique ou pas, pareil pour leurs valeurs.

*/


var a = NaN;
var b = Infinity;

console.log(typeof(a));
console.log(typeof(b));

function compare(a,b) {
    if (typeof(a) != typeof(b)) {
        console.log("Type différrent")
    }
    else {
        console.log("Type identique")
    }

    if (a != b) {
        console.log("Valeur différente")
    }
    else {
        console.log("Valeur identique")
    }
}

compare(a,b);