/*

Consignes de l'exercice

- Comptez le nombre de croix dans les tableaux
- Convertir ce programme en une fonction (qui sera appelée 4 fois).

*/

/* Premier tableau

var tab0 = ["X", "O", "O", "X", "Z", 6, null, "X"];
var stock = 0

for (var i = 0; i < tab0.length; i++) {
	if (tab0[i] == "X") {
		stock = stock+1;
	}
}
console.log("Il y a "+stock+" X dans le tableau 0");

*/

/* Second tableau 

var tab1 = [
	["X", "O", "X"],
	["O", "O", "X"],
	["X", "O", "O"]
];

var stock = 0

for (var i = 0; i < tab1.length; i++) {
	for (var j = 0; j <tab1.length; j++) {
		if (tab1[i][j] == "X") {
			stock = stock+1
		}		
	}
}

console.log("Il y a " + stock + " X dans le tableau 1");

*/

/* Troisième tableau 

var tab2 = [
	["X" ,"O", "X"],
	["O" ,"O", "X"],
	["X" ,"O", "O", "Z"]
];

var stock = 0;

for (var i = 0; i<tab2.length; i++) {
	for (var j = 0; j<=tab2.length; j++) {
		if (tab2[i][j] == "X"){
      		stock = stock + 1;
    	}
	}
}
console.log(tab2);
console.log("Il y a " + stock + " X dans le tableau 2");

*/

/* Quatrième tableau 

var tab3 = [
	[ ["X","R"], ["T","Z"], ["X","Y"], ["U","F"] ],
	[ ["Q","R"], ["X","Z"], ["X","Y"], ["X","F"] ],
	[ ["V","S"], ["T","X"], ["O","Y"], ["X","F"] ],
	[ ["E","X"], ["Q","Z"], ["X","Y"], ["U","F"] ]
];

var stock = 0;

for (var i = 0; i < tab3.length; i++) {
	for (var j = 0; j < tab3.length; j++) {
		for (var k = 0; k < tab3.length; k++) {
			if (tab3[i][j][k] == "X"){
				stock = stock + 1;
			}
		}
	}
}

console.log(tab3);
console.log("Il y a " + stock + " X dans le tableau 3");

*/