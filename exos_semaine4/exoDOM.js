/* Étape 1 */

var texte = document.createElement('p');
texte.id = "texte";
document.getElementById("root").appendChild(texte);
document.getElementById("texte").innerHTML = "Lorem ipsum dolor sit amet consectetur.";

var liste = document.createElement('ul');
liste.id = "liste";
document.getElementById("root").appendChild(liste);

for (var i = 0; i < 5; i++) {
    var ligne = document.createElement('li');
    document.getElementById('liste').appendChild(ligne);
    var textePoint = document.createTextNode("Lorem ipsum dolor sit, amet consectetur adipisicing elit.")
    ligne.appendChild(textePoint);
}

/* Étape 2 */

