/*

Créer une fonction qui inverse les absisses et les ordonnées, par exemple :

*/

var tab1 = [
	["A", "B", "C"],
	[ 1,   2,   3 ],
	[true, false, false]
];

devient :

var tab2 = [
	[true,  1, "A"],
	[false, 2, "B"],
	[false, 3, "C"]
];
