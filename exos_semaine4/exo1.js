/*

Consignes de l'exercice :

- Afficher "oui" si le prénom saisi fait plus de 8 caractères, 
- Afficher "non" en cas contraire.
- Afficher "Banco !", si le prénom saisi fait plus de 10 caractère.
- Afficher "Bonjour " + le prénom

*/

var nom = prompt("Saisissez votre nom :");

if (nom.length >= 10) {
    console.log("Banco !");
}
else if (nom.length <= 8) {
    console.log("non");
}
else {
    console.log("oui");
}

console.log("Bonjour " + nom);